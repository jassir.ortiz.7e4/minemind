package com.example.minemind

data class Carta(
    val id: Int,
    var resId: Int,
    var pareja: Boolean = false,
    var girada: Boolean = false
)