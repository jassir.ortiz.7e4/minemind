package com.example.minemind

import android.os.CountDownTimer
import android.widget.ImageView
import androidx.lifecycle.ViewModel

class GameHardViewModel : ViewModel() {
    // Properties
    var firstClick: Int? = null
    var firstImage: ImageView? = null
    var secondClick: Int? = null
    var secondImage: ImageView? = null
    var text = ""
    var counter = -1
    var cards = arrayOf(
        R.drawable.image1,
        R.drawable.image2,
        R.drawable.image3,
        R.drawable.image4,
        R.drawable.image1,
        R.drawable.image2,
        R.drawable.image3,
        R.drawable.image4
    )

    lateinit var timer: CountDownTimer
    var counterTimer = 0
    var timerStart = 0
    var cartas = mutableListOf<Carta>()
    var finishGame: Boolean = false

    init {
        setDataModel()
        timerCount()
    }

    private fun setDataModel() {
        cards.shuffle()
        for (i in 0..7) {
            cartas.add(Carta(i, cards[i], false))
        }
    }

    fun girarCarta(idCarta: Int): Int {
        return if (!cartas[idCarta].girada) {
            cartas[idCarta].girada = true
            cartas[idCarta].resId
        } else {
            cartas[idCarta].girada = false
            R.drawable.cardreverse
        }
    }

    fun valueCard(i: Int, carta: ImageView) {
        val firstInt: Int
        val secondInt: Int
        if (firstClick == null) {
            println("ENTRO PRIMER CLICK")
            firstClick = i
            firstImage = carta
            firstImage?.isEnabled = false
        } else if (secondClick == null && firstClick != null) {
            secondClick = i
            secondImage = carta
            firstInt = firstClick!!
            secondInt = secondClick!!
            println("ENTRO SEGUNDO CLICK")
            println("Int: $firstInt  $secondInt")
            if (cartas[firstInt].resId == cartas[secondInt].resId) {
                println("ENTRO CARTAS IGUALES")
                cartas[firstInt].pareja = true
                cartas[secondInt].pareja = true
                firstImage?.isEnabled = false
                secondImage?.isEnabled = false
                valueCards()
                clear()
            } else {
                println("ENTRO ELSE")
                firstImage!!.setImageResource(girarCarta(firstInt))
                secondImage!!.setImageResource(girarCarta(secondInt))
                firstImage?.isEnabled = true
                clear()
            }
        }
    }

    fun clear() {
        firstClick = null
        secondClick = null
        firstImage = null
        secondImage = null
    }

    fun estadoCarta(idCarta: Int): Int {
        if (cartas[idCarta].girada) return cartas[idCarta].resId
        else return R.drawable.cardreverse
    }

    fun countMoves(): String {
        counter++
        text = ("Movements: $counter").toString()
        return text
    }

    fun valueCards() {
        if (cartas[0].pareja && cartas[1].pareja && cartas[2].pareja && cartas[3].pareja && cartas[4].pareja && cartas[5].pareja && cartas[6].pareja && cartas[7].pareja) {
            finishGame = true
        }
    }

    fun timerCount(): CountDownTimer {
        timer = object : CountDownTimer(300000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (counterTimer == 60) {
                    timerStart += 1
                    counterTimer = 0
                } else {
                    counterTimer++
                }
                valueCards()
            }

            override fun onFinish() {
                finishGame = true
            }
        }.start()
        if (finishGame) {
            timer.cancel()
        }
        return timer
    }
}
