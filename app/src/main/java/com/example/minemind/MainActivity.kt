package com.example.minemind

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    // Properties
    lateinit var playButton: Button
    lateinit var helpButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        helpButton = findViewById(R.id.button_help)
        playButton = findViewById(R.id.button_play)

        playButton.setOnClickListener { callGameActivity() }
        helpButton.setOnClickListener { helpConten() }
    }

    private fun callGameActivity() {
        val callActivityGame = Intent(this, LevelDifficulty::class.java)
        startActivity(callActivityGame)
    }

    fun helpConten(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.helpPlay)
        builder.setMessage(R.string.helpPlayContent)
        builder.setIcon(R.drawable.iconoapp)
        builder.setPositiveButton("Ok"){dialogInterface, which ->
            Toast.makeText(applicationContext,"Enjoy the game",Toast.LENGTH_LONG).show()
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
}