package com.example.minemind

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class ResultScreen : AppCompatActivity() {
    lateinit var buttonPlayAgain: Button
    lateinit var buttonMenu: Button
    lateinit var textPoints: TextView
    var score = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rusultscreen)

        val callActivityGame = Intent(this, GameScreen::class.java)
        val callActivityMenu = Intent(this, MainActivity::class.java)
        textPoints = findViewById(R.id.textPoints)
        buttonMenu = findViewById(R.id.button_menu)
        buttonPlayAgain = findViewById(R.id.button_playAgain)

        setScore()

        buttonMenu.setOnClickListener { startActivity(callActivityMenu) }
        buttonPlayAgain.setOnClickListener { startActivity(callActivityGame) }
    }

    private fun setScore() {
        score = intent.getIntExtra("score", score)
        if (score < 0) {
            score = 0
        }
        textPoints.text = "$score"
    }
}