package com.example.minemind

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class GameScreen : AppCompatActivity(), View.OnClickListener {
    // Properties
    lateinit var textCounter: TextView
    lateinit var textTimer: TextView
    private lateinit var carta1: ImageView
    private lateinit var carta2: ImageView
    private lateinit var carta3: ImageView
    private lateinit var carta4: ImageView
    private lateinit var carta5: ImageView
    private lateinit var carta6: ImageView

    var counterTimer = 0
    var timerStart = 0
    lateinit var timer: CountDownTimer

    private lateinit var viewModel: GameViewModel

    // Main
    override fun onCreate(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_screen)

        assignImage()
        timerCount()

        textCounter = findViewById(R.id.textMovements)
        textTimer = findViewById(R.id.textCounter)

        carta1.setOnClickListener(this)
        carta2.setOnClickListener(this)
        carta3.setOnClickListener(this)
        carta4.setOnClickListener(this)
        carta5.setOnClickListener(this)
        carta6.setOnClickListener(this)

        updateUI()
    }

    // Methods
    fun assignImage() {
        carta1 = findViewById(R.id.carta1)
        carta2 = findViewById(R.id.carta2)
        carta3 = findViewById(R.id.carta3)
        carta4 = findViewById(R.id.carta4)
        carta5 = findViewById(R.id.carta5)
        carta6 = findViewById(R.id.carta6)
    }

    override fun onClick(v: View?) {
        textCounter.text = viewModel.countMoves()

        when (v) {
            carta1 -> girarCarta(0, carta1)
            carta2 -> girarCarta(1, carta2)
            carta3 -> girarCarta(2, carta3)
            carta4 -> girarCarta(3, carta4)
            carta5 -> girarCarta(4, carta5)
            carta6 -> girarCarta(5, carta6)
        }
    }

    private fun girarCarta(i: Int, carta: ImageView) {
        carta.setImageResource(viewModel.girarCarta(i))
        viewModel.valueCard(i, carta)
        if(viewModel.finishGame) finishGame()
    }

    /*fun valueCard() {
        for (i in 0..5) {
            if (viewModel.cartas[i].girada) {
                for (j in 0..5) {
                    if (j != i) {
                        if (viewModel.cartas[j].girada) {
                            if (viewModel.cartas[j].resId == viewModel.cartas[i].resId) {
                                viewModel.cartas[j].pareja = true
                                viewModel.cartas[i].pareja = true
                            }
                        }
                    }
                }
            }
        }
        if (viewModel.finishGame) {
            finishGame()
        }
    }*/

    fun finishGame() {
        val score = 5000 - (timerStart + counterTimer * viewModel.counter)
        timer.cancel()
        viewModel.timer.cancel()
        val callActivityGame = Intent(this, ResultScreen::class.java)
        callActivityGame.putExtra("score", score)
        startActivity(callActivityGame)
    }
    fun timerCount() {
        timer = object : CountDownTimer(120000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                textTimer.text = ("0${timerStart}:${counterTimer}")
                if (counterTimer == 60) {
                    timerStart += 1
                    counterTimer = 0
                } else {
                    counterTimer++
                }
                viewModel.valueCards()
            }
            override fun onFinish() {
                finishGame()
            }
        }.start()
    }

    private fun updateUI() {
        carta1.setImageResource(viewModel.estadoCarta(0))
        carta2.setImageResource(viewModel.estadoCarta(1))
        carta3.setImageResource(viewModel.estadoCarta(2))
        carta4.setImageResource(viewModel.estadoCarta(3))
        carta5.setImageResource(viewModel.estadoCarta(4))
        carta6.setImageResource(viewModel.estadoCarta(5))
        textCounter.text = viewModel.countMoves()
        timerStart = viewModel.timerStart
        counterTimer = viewModel.counterTimer
    }
}