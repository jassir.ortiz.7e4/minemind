package com.example.minemind

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class LevelDifficulty : AppCompatActivity() {

    lateinit var buttonHard: Button
    lateinit var buttonNormal: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.level_difficulty)

        buttonNormal = findViewById(R.id.button_normal)

        val callActivityGame = Intent(this, GameScreen::class.java)
        buttonNormal.setOnClickListener { startActivity(callActivityGame) }

        buttonHard = findViewById(R.id.button_hard)
        val callActivityGameHardScreen = Intent(this, GameHardScreen::class.java)
        buttonHard.setOnClickListener { startActivity(callActivityGameHardScreen) }
    }
}